%%%-------------------------------------------------------------------
%%% @author tjajakop
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 12. Mar 2019 21:54
%%%-------------------------------------------------------------------
-module(tut).
-author("tjajakop").

%% API
-export([double/1]).

double(X) ->
  2 * X.