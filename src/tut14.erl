%%%-------------------------------------------------------------------
%%% @author tjajakop
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Mar 2019 16:40
%%%-------------------------------------------------------------------
-module(tut14).
-author("tjajakop").

%% API
-export([start/0, say_something/2]).

say_something(What, 0) ->
  done;
say_something(What, Times) ->
  io:format("~p~n", [What]),
  say_something(What, Times - 1).

start() ->
  spawn(tut14, say_something, [hello, 3]),
  spawn(tut14, say_something, [goodbye, 3]).