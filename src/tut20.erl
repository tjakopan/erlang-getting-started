%%%-------------------------------------------------------------------
%%% @author tjajakop
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Mar 2019 09:57
%%%-------------------------------------------------------------------
-module(tut20).
-author("tjajakop").

%% API
-export([start/1, ping/2, pong/0]).

ping(N, PongPid) ->
  link(PongPid),
  ping1(N, PongPid).

ping1(0, _) ->
  exit(ping);
ping1(N, PongPid) ->
  PongPid ! {ping, self()},
  receive
    pong -> io:format("Ping received pong~n", [])
  end,
  ping1(N - 1, PongPid).

pong() ->
  receive
    {ping, PingPid} ->
      io:format("Pong received ping~n", []),
      PingPid ! pong,
      pong()
  end.

start(PingNode) ->
  PongPid = spawn(tut20, pong, []),
  spawn(PingNode, tut20, ping, [3, PongPid]).