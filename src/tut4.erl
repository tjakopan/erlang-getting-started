%%%-------------------------------------------------------------------
%%% @author tjajakop
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 12. Mar 2019 22:15
%%%-------------------------------------------------------------------
-module(tut4).
-author("tjajakop").

%% API
-export([list_length/1]).

list_length([]) ->
  0;
list_length([First | Rest]) ->
  1 + list_length(Rest).