%%%-------------------------------------------------------------------
%%% @author tjajakop
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 12. Mar 2019 22:52
%%%-------------------------------------------------------------------
-module(tut6).
-author("tjajakop").

%% API
-export([list_max/1]).

list_max([Head | Rest]) ->
  list_max(Rest, Head).

list_max([], Res) ->
  Res;
list_max([Head | Rest], Result_so_far) when Head > Result_so_far ->
  list_max(Rest, Head);
list_max([Head | Rest], Result_so_far) ->
  list_max(Rest, Result_so_far).