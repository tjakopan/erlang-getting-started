%%%-------------------------------------------------------------------
%%% @author tjajakop
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Mar 2019 09:46
%%%-------------------------------------------------------------------
-module(tut19).
-author("tjajakop").

%% API
-export([start_ping/1, start_pong/0, ping/2, pong/0]).

ping(0, PongNode) ->
  io:format("Ping finished~n", []);
ping(N, PongNode) ->
  {pong, PongNode} ! {ping, self()},
  receive
    pong -> io:format("Ping received pong~n", [])
  end,
  ping(N - 1, PongNode).

pong() ->
  receive
    {ping, PingPID} ->
      io:format("Pong received ping~n", []),
      PingPID ! pong,
      pong()
  after 5000 -> io:format("Pong timed out~n", [])
  end.

start_pong() ->
  register(pong, spawn(tut19, pong, [])).

start_ping(PongNode) ->
  spawn(tut19, ping, [3, PongNode]).