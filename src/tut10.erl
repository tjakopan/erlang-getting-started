%%%-------------------------------------------------------------------
%%% @author tjajakop
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Mar 2019 14:03
%%%-------------------------------------------------------------------
-module(tut10).
-author("tjajakop").

%% API
-export([convert_length/1]).

convert_length(Length) ->
  case Length of
    {centimeter, X} ->
      {inch, X / 2.54};
    {inch, Y} ->
      {centimeter, Y * 2.54}
  end.