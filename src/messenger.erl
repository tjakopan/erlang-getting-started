%%%-------------------------------------------------------------------
%%% @author tjajakop
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Mar 2019 08:05
%%%-------------------------------------------------------------------
-module(messenger).
-author("tjajakop").

%% API
-export([start_server/0, server/0, logon/1, logoff/0, message/2, client/2]).

server_node() ->
  messenger@5CD806BZPN.

%%% This is the server process for the "messenger"
%%% the user list has the format [{ClientPid1, Name1},{ClientPid22, Name2},...]
server() ->
  process_flag(trap_exit, true),
  server([]).

server(UserList) ->
  receive
    {From, logon, Name} ->
      NewUserList = server_logon(From, Name, UserList),
      server(NewUserList);
    {'EXIT', From, _} ->
      NewUserList = server_logoff(From, UserList),
      server(NewUserList);
    {From, message_to, To, Message} ->
      server_transfer(From, To, Message, UserList),
      io:format("list is now: ~p~n", [UserList]),
      server(UserList)
  end.

%%% Start the server
start_server() ->
  register(messenger, spawn(messenger, server, [[]])).

%%% Server adds a new user to the list
server_logon(From, Name, UserList) ->
  %% check if logged on anywhere else
  case lists:keymember(Name, 2, UserList) of
    true ->
      From ! {messenger, stop, user_exists_at_other_node}, %% reject
      UserList;
    false ->
      From ! {messenger, logged_on},
      [{From, Name} | UserList] %% add user to the list
  end.

%%% Server deletes a user from the user list
server_logoff(From, UserList) ->
  lists:keydelete(From, 1, UserList).

%%% Server transfers a message between user
server_transfer(From, To, Message, UserList) ->
  %% check that the user is logged on and who he is
  case lists:keysearch(From, 1, UserList) of
    false -> From ! {messenger, stop, you_are_not_logged_on};
    {value, {_, Name}} ->
      server_transfer(From, Name, To, Message, UserList)
  end.

%%% If user exists, send the message
server_transfer(From, Name, To, Message, UserList) ->
  %% Find the receiver and send the message
  case lists:keysearch(To, 2, UserList) of
    false -> From ! {messenger, receiver_not_found};
    {value, {ToPid, To}} ->
      ToPid ! {message_from, Name, Message},
      From ! {messenger, sent}
  end.

%%% User commands
logon(Name) ->
  case whereis(mess_client) of
    undefined -> register(mess_client, spawn(messenger, client, [server_node(), Name]));
    _ -> already_logged_on
  end.

logoff() ->
  mess_client ! logoff.

message(ToName, Message) ->
  case whereis(mess_client) of %% Test if the client is running
    undefined -> not_logged_on;
    _ ->
      mess_client ! {message_to, ToName, Message},
      ok
  end.

%%% The client process which runs on each server node
client(ServerNode, Name) ->
  {messenger, ServerNode} ! {self(), logon, Name},
  await_result(),
  client(ServerNode).

client(ServerNode) ->
  receive
    logoff -> exit(normal);
    {message_to, ToName, Message} ->
      {messenger, ServerNode} ! {self(), message_to, ToName, Message},
      await_result();
    {message_from, FromName, Message} ->
      io:format("Message from ~p: ~p~n", [FromName, Message])
  end,
  client(ServerNode).

%%% Wait from a response from the server
await_result() ->
  receive
    {messenger, stop, Why} -> % Stop the client
      io:format("~p~n", [Why]),
      exit(normal);
    {messenger, What} -> % Normal response
      io:format("~p~n", [What])
  after 5000 ->
    io:format("No response from server~n", []),
    exit(timeout)
  end.

