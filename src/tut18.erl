%%%-------------------------------------------------------------------
%%% @author tjajakop
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Mar 2019 08:00
%%%-------------------------------------------------------------------
-module(tut18).
-author("tjajakop").

%% API
-export([start/1, ping/2, pong/0]).

ping(0, Pong_Node) ->
  {pong, Pong_Node} ! finished,
  io:format("ping finished~n", []);
ping(N, Pong_Node) ->
  {pong, Pong_Node} ! {ping, self()},
  receive
    pong -> io:format("Ping received pong~n", [])
  end,
  ping(N - 1, Pong_Node).

pong() ->
  receive
    finished -> io:format("Pong finished~n", []);
    {ping, Ping_PID} ->
      io:format("Pong received ping~n", []),
      Ping_PID ! pong,
      pong()
  end.

start(Ping_Node) ->
  register(pong, spawn(tut18, pong, [])),
  spawn(Ping_Node, tut18, ping, [3, node()]).